# Focus Movement Fix
For use with **LabyMod 1.8.9 only**.  
This is a port of the [Portal Input Fix](https://github.com/RoccoDev/PortalInputFix) mod.

This addon backports a fix from Minecraft 1.12 that lets you input movement actions
right after closing GUIs.  
Since the "level download" screen is a GUI, this also fixes broken controls after entering
portals or changing worlds/dimensions (e.g. switching lobbies).

## Disclaimer
**This addon might <ins>not</ins> be allowed in every server.**

Since playing in 1.12 would basically give you the same experience, we'd recommend using this mod
**only on servers that support 1.12**.  
We're not responsible for any punishments related to this addon.

## Mod Conflicts
See [MOD-DEVELOPERS.md](MOD-DEVELOPERS.md) for info on how this addon works.  
If you're a mod developer and this causes a conflict with your mod, feel free to open an issue.
